
package src.pt.ulusofona.lp2.jungleGame.Animais;


import org.junit.Test;
import static org.junit.Assert.*;
import pt.ulusofona.lp2.jungleGame.Animais.Girafa;


public class GirafaTest {
    


    @Test
    public void testMove() {
        System.out.println("move leao");
         int posicao=0;
        int maxPosicao = 12;
        char terreno = 'T';
        Girafa instance = new Girafa("leao",3,"7",1,3,4);
        int expResult = 2;
        char[] terrenos = new char[]{'T','T','T','T','A','A','T','T','R','R','L','T'};
        boolean result = instance.move(maxPosicao, terreno);
        assertEquals(expResult, instance.getPosicao());
     
    }
    
    @Test
    public void testMoveA() {
        System.out.println("move leao");
         int posicao=0;
        int maxPosicao = 12;
        char terreno = 'T';
        Girafa instance = new Girafa("leao",3,"7",2,4,4);
        int expResult = 5;
        char[] terrenos = new char[]{'T','T','T','T','A','A','T','T','R','R','L','T'};
        boolean result = instance.move(maxPosicao, terreno);
        assertEquals(expResult, instance.getPosicao());
     
    }
    
}
