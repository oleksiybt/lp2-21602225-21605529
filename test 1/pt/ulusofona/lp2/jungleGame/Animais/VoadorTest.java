
package pt.ulusofona.lp2.jungleGame.Animais;


import org.junit.Test;
import static org.junit.Assert.*;


public class VoadorTest {
    
 

    @Test
    public void testMove() {
        System.out.println("move");
        int posicao=0;
        int maxPosicao = 12;
        char terreno = 'R';
        Voador instance = new Voador("Voador",7,"4",6,3,3);
        char[] terrenos = new char[]{'T','T','T','T','A','A','T','T','R','R','L','T'};
        int expResult = 10;
        boolean result = instance.move(maxPosicao, terreno);
        assertEquals(expResult, instance.getPosicao());
        
    }
    
}
