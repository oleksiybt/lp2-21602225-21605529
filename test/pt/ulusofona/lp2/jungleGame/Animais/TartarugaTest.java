/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ulusofona.lp2.jungleGame.Animais;


import org.junit.Test;
import static org.junit.Assert.*;

public class TartarugaTest {
 

    @Test
    public void testMoveT() {
        System.out.println("move Terra");
       int posicao=0;
        int maxPosicao = 12;
        char terreno = 'T';
        Tartaruga instance = new Tartaruga("Tartaruga",3,"7",1,3,4);
        char[] terrenos = new char[]{'T','T','T','T','A','A','T','T','R','R','L','T'};
        int  expResult = 2;
        boolean resultBoolean = instance.move(maxPosicao, terreno);
        assertEquals(expResult, instance.getPosicao());
    }
    @Test
    public void testMoveL() {
        System.out.println("move Lama");
       int posicao=0;
        int maxPosicao = 12;
        char terreno = 'L';
        Tartaruga instance = new Tartaruga("Tartaruga",3,"7",1,3,4);
        char[] terrenos = new char[]{'T','T','T','T','A','A','T','T','R','R','L','T'};
        int  expResult = 1;
        boolean resultBoolean = instance.move(maxPosicao, terreno);
        assertEquals(expResult, instance.getPosicao());
    }
    
    @Test
    public void testMoveA() {
        System.out.println("move Agua");
       int posicao=0;
        int maxPosicao = 12;
        char terreno = 'A';
        Tartaruga instance = new Tartaruga("Tartaruga",3,"7",1,3,4);
        char[] terrenos = new char[]{'T','T','T','T','A','A','T','T','R','R','L','T'};
        int  expResult = 4;
        boolean resultBoolean = instance.move(maxPosicao, terreno);
        assertEquals(expResult, instance.getPosicao());
    }
     @Test
    public void testMoveAT() {
        System.out.println("move Agua");
       int posicao=0;
        int maxPosicao = 12;
        char terreno = 'A';
        Tartaruga instance = new Tartaruga("Tartaruga",3,"7",1,3,4);
        char[] terrenos = new char[]{'T','T','T','T','A','A','T','T','R','R','L','T'};
        int  expResult = 4;
        boolean resultBoolean = instance.move(maxPosicao, terreno);
        assertEquals(expResult, instance.getPosicao());
    }
}
