package pt.ulusofona.lp2.jungleGame;


public class Equipa implements Comparable<Equipa>{//Sorry for hardcode :\ a entrega termina em 30 mins
    private int id;
    private String nome;
    private int totalPoints;
    private int elementosClassificados;
    
    
    public boolean addPoints(int value){
        if(elementosClassificados==2){
            return false;
        }
        elementosClassificados++;
        totalPoints+=value;
        return true;
    }
    
    public int getPoints(){
        return totalPoints;
    }
    
    public int getID(){
        return id;
    }
    
   @Override
   public int compareTo(Equipa outro){
       if(outro.totalPoints==this.totalPoints){
           return Favoritismo.getFavoritismo().getFavoritismoValor(outro.id)-Favoritismo.getFavoritismo().getFavoritismoValor(this.id);
       }else{
            return outro.totalPoints-this.totalPoints;
       }
   }
   
   @Override
   public String toString(){
       return nome + ", " + totalPoints +" pts";
   }
   
    public Equipa(int id,String nome){
        this.id=id;
        this.nome=nome;
        totalPoints=0;
        elementosClassificados=0;
    }
}
