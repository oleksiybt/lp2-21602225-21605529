package pt.ulusofona.lp2.jungleGame;

import java.util.HashMap;

public class Terreno {
    private static Terreno terenoAtual = new Terreno();
    
    private HashMap<Character, String> terrenoIconMap = new HashMap<>();
    private char[] terreno;
    
    public static Terreno getTerreno(){
        return terenoAtual;
    }
    
    public char[] getTodoTerreno(){
        return terreno;
    }
    public String getIcon(int posicao){
        return terrenoIconMap.get(getTerrenoByPosition(posicao));
    }
    
    public String getIcon(){
        return terrenoIconMap.get('T');
    }
    
    public int getAtrito(char letra){
        switch(letra){
            case 'A':
            case 'L':
            case 'R':
                return 1;
            case 'T':
            case 'M':
                return 0;
            default:
                throw new IllegalArgumentException("Terreno inválido ("+letra+")");
        }
    }
    public char getTerrenoByPosition(int posicao){
        return terreno[posicao];
    }
    public void setTerreno(char[] terreno){
        this.terreno=terreno;
    }
    
    private Terreno(){
        terrenoIconMap.put('A', "box-water.png");
        terrenoIconMap.put('T', "box-land.png");
        terrenoIconMap.put('R', "box-mud.png");
        terrenoIconMap.put('L', "box-rocks.png");
        terrenoIconMap.put('M', "box-land.png");
    }
    
    
}
