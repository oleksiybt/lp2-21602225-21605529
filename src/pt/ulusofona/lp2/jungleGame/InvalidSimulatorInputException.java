package pt.ulusofona.lp2.jungleGame;

import java.io.IOException;

public class InvalidSimulatorInputException extends IOException{
    private int line;
    
    
    public int getInputLine(){
        return line;
    }
    
    public InvalidSimulatorInputException(int line){
        this.line=line;
    }
}