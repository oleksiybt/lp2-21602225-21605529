package pt.ulusofona.lp2.jungleGame;

public class Premio {
    private String nome;
    private int posicao;
    private int valor;
    private boolean acumulavel;
    
    
    /////////////////////////////////////Getters & Setters\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    public int getValor(){
        return valor;
    }
    public int getposicao(){
        return posicao;
    }
    public boolean isAcumulavel(){
        return acumulavel;
    }
    @Override
    public String toString(){
        return nome + " (" + valor + ")";
    }
    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\///////////////////////////////////////////////////////
    
    
    
    
    
    ////////////////////////////////////////Construtores\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    public Premio(String nome, int valor, int posicao, boolean acumulavel){
        this.nome = nome;
        this.posicao = posicao;
        this.valor = valor;
        this.acumulavel = acumulavel;
    }
    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\///////////////////////////////////////////////////////   
}
