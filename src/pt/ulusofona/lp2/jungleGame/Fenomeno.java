package pt.ulusofona.lp2.jungleGame;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;


public class Fenomeno {
    private static Fenomeno instance;
    private ArrayList<Integer>[] tempo;
    List<Animal> animaisBackup = new ArrayList<>();
    Animal animalBackup;
    private final Consumer<Animal>[] fenomenosIDMap = new Consumer[] { // Verificar o caso da tartaruga!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        (Consumer<Animal>)(null),//Indice 0 - Nao faz nada
        (Consumer<Animal>)((Animal a) -> a.addInteligencia(a.getInteligencia()>1?-1:0)), //Indice 1 - Baixa de Oxigenio
        (Consumer<Animal>)((Animal a) -> a.addEnergia(2 * a.getEnergiaAtual())), //Indice 2 - doping
        (Consumer<Animal>)((Animal a) -> a.addVelocidadeBase(1)), //Indice 3 - ventoAFavor
        (Consumer<Animal>)((Animal a) -> a.addVelocidadeBase(a.getVelocidadeBase()>1?-1:0)), //Indice 4 - ventoContra - repeticao do codigo da baixa de oxigenio
        (Consumer<Animal>)((Animal a) -> a.addVelocidadeBase(a.getVelocidadeBase()>4?-2:0)) //Indice 5 - espiritoSolidario
    };
    
    public boolean fenomenoExiste(Integer tempoAtual){
        return tempoAtual<tempo.length && tempo[tempoAtual] != null;
    }
            
    public void addFenomenos(int tempoDoFenomeno, int fenomenoID){
        if(this.tempo.length <= tempoDoFenomeno){
            ArrayList<Integer>[] temp = new ArrayList[tempoDoFenomeno+1];
            System.arraycopy(tempo, 0, temp, 0, tempo.length);
            tempo = temp;
        }
        if(tempo[tempoDoFenomeno] == null){
            tempo[tempoDoFenomeno] = new ArrayList<>();
        }
        tempo[tempoDoFenomeno].add(fenomenoID);
    }
    
    public void aplicaFenomeno(Animal animal, Integer tempoAtual){
        if(fenomenoExiste(tempoAtual)){
            guardaAnimal(animal);
            for(Integer id : tempo[tempoAtual]){
                fenomenosIDMap[id].accept(animal);
            }
        }
    }
    
    private void guardaAnimal(Animal animal){
        try{
            animalBackup = animal.clone();
        }catch(CloneNotSupportedException e){
            System.out.println(e.getMessage()); 
        }
    }

    public void reverterAnimal(Animal animal, Integer tempoAtual){
        if(fenomenoExiste(tempoAtual))
        {
            if(animal.getEnergiaFoiAlterada()){
                animal.setEnergia(animalBackup.getEnergiaAtual());
            }
            animal.setInteligencia(animalBackup.getInteligencia());
            animal.setVelocidadeBase(animalBackup.getVelocidadeBase());
        }
    }
     
    public void clearData(){
        tempo = new ArrayList[0];
        animaisBackup.clear();
    }
    
    public static Fenomeno getInstance(){
        if(instance == null){
            instance = new Fenomeno();
        }
        return instance;
    }
    
    private Fenomeno(){
        tempo = new ArrayList[0];
    }
    
}
