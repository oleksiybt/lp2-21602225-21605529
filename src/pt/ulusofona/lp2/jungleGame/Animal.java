
package pt.ulusofona.lp2.jungleGame;

import java.util.ArrayList;
import java.util.List;

public abstract class Animal implements Cloneable{
    private int idEspecie;
    private String nome;
    private String especie;
    private int energiaInicial; ///////////////////////verificar a utilidade desta variavel!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    private int tempoChegada;
    private boolean energiaFoiAlterada;
    
    protected int velocidadeBase; // pode ser modificado pelo fenomeno
    protected int energiaActual; // pode ser modificado pelo fenomeno
    protected int posicao; 
    protected int inteligencia; // pode ser modificado pelo fenomeno
    
    private List<Premio> premios;
    //protected abstract int getFavoritismo();//utilizado para obter o favoritismo das classes child
    
    
    public int getPosicao(){
        return posicao;
    }
    public List<Premio> getPremios(){
        return premios;
    }
    public String getImagePNG(){
        return null;
    }
    public String getNome(){
        return nome;
    }
    public int getInteligencia(){
        return inteligencia;
    }
    public int getVelocidadeBase(){
        return velocidadeBase;
    }
    public int getEnergiaAtual(){
        return energiaActual;
    }
    public int getPremiosTotalValue(){
        int valor=0;
        
        for(Premio p:premios){
            valor += p.getValor();
        }
        return valor;
    }
    public boolean getEnergiaFoiAlterada(){
        return energiaFoiAlterada;
    }
    public int getFavoritismo(){
        return Favoritismo.getFavoritismo().getFavoritismoValor(idEspecie);
    }
    public int getTempoChegada(){
        return tempoChegada;
    }
    public int getEnergiaInicial(){
        return this.energiaInicial;
    }
    public void setEnergiaFoiAlterada(boolean alteracao){
        this.energiaFoiAlterada=alteracao;
    }
    public void addInteligencia(int inteligencia){
        this.inteligencia += inteligencia;
    }
    public void addVelocidadeBase(int velocidade){
        this.velocidadeBase += velocidade;
    }
    public void addEnergia(int energia){
        this.energiaActual += energia;
        energiaFoiAlterada = true;
    }
    
    public void setInteligencia(int inteligencia){
        this.inteligencia = inteligencia;
    }
    public void setVelocidadeBase(int velocidadeBase){
        this.velocidadeBase = velocidadeBase;
    }
    public void setEnergia(int energia){
        this.energiaActual = energia;
    }
    
    public void setTempoChegada(int tempoChegada){
         this.tempoChegada = tempoChegada;
    }
    
    public void addPremio(Premio premio){
        premios.add(premio);
    }
    public boolean checkPremiosAcumulaveis(){//verifica se tem pelo menos 1 premio acumulavel
        for(Premio p : premios){
            if(p.isAcumulavel()){
                return true;
            }
        }
        return false;
    }
    
    public int getID(){
        return this.idEspecie;
    }
    public String getEspecie(){
        return this.especie;
    }
   
    
    
    @Override
    public String toString(){
        return (nome + ", " +  especie + ", t" + tempoChegada) ;
    }
    
    
    public abstract boolean move(int maxPosicao, char terreno);
 
    
    protected boolean ProcessaEnergia(int distancia){
        if(distancia != 0 && energiaActual>0){//caso não se mova // se a distância do salto for 0, mas consumir energia, ficará num parado no citio para sempre // como ja aconteceu qundo o gorila se afogou na agua e o simba na lama RIP
            return true;
        }
        else{
            energiaActual += Math.max(Math.min(inteligencia*1.0/2, energiaInicial), 1);
            return false;
        }
    }
    protected void ConsomeEnergia(int distancia){
        int temp =(int)(distancia-inteligencia*1.0/2);
        energiaActual -= energiaActual > 0 ? Math.max(temp,1):0;
        energiaActual = Math.max(energiaActual, 0);
    }
    
   public void removePremiosNaoAcumulaveis(){
       List<Premio> temp = new ArrayList<Premio>();
       for(Premio p: premios){
           if(!p.isAcumulavel()){
               temp.add(p);
           }
       }
       premios.removeAll(temp);
   }
   
   public void reterOMaiorPremio(){
    for(int ultimo = premios.size()-1; ultimo > 0 ;ultimo--){//Assumindo que não há empates //
        if(premios.get(0).getValor() > premios.get(ultimo).getValor()){
            premios.remove(ultimo);
        }else{
            premios.remove(0);
        }
    }
   }
   @Override
   public Animal clone() throws CloneNotSupportedException{
       return (Animal)super.clone();
   }   
    
    public Animal() {
    }
    public Animal(String nome, int idEspecie, String especie ,int velocidadeBase ,int energia, int inteligencia){
        this.idEspecie=idEspecie;
        this.energiaInicial = this.energiaActual=energia;
        this.velocidadeBase = velocidadeBase;
        this.especie = especie;
        this.nome = nome;
        this.inteligencia = inteligencia;
        energiaFoiAlterada = false;
        posicao = 0;
        premios = new ArrayList<>();
    }
    
    public void debug(){
        System.out.println(" nome: " + nome + "; energia: " + energiaInicial +"/"+ energiaActual + "; velocidade base:" + velocidadeBase + "; inteligencia:" + inteligencia + /*"   salto: " + distancia +*/ "   posicao: " + posicao);
    }  
}