package pt.ulusofona.lp2.jungleGame;

import java.util.HashMap;

public class Especies {
    private HashMap<Integer, String> especiesMap = new HashMap<>();
    
    public String getEspecieByID(int id){
        return especiesMap.get(id);
    }
    
    public Especies(){
        especiesMap.put(0, "Águia");
        especiesMap.put(1, "Girafa");
        especiesMap.put(2, "Gorila");
        especiesMap.put(3, "Humano");
        especiesMap.put(4, "Leão");
        especiesMap.put(5, "Lebre");
        especiesMap.put(6, "Mocho");
        especiesMap.put(7, "Tartaruga");
        especiesMap.put(8, "Tigre");
        especiesMap.put(9, "Zebra");
    }
}
