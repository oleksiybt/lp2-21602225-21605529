
package pt.ulusofona.lp2.jungleGame;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;


public class Simulador {

    private List<Animal> animais;
    private List<Premio> premios;
    private int comprimentoPista;
    private Especies especies;
    private int participantsCount;
    private int tempCount;
    List<Equipa> equipas;
    //private char[] terreno;
    Fenomeno fenomenos;
    private List<String> outputStringList;
    
    //Getters, setters, INs & OUT's
    public List<Animal> getAnimais(){
        return animais;
    }
    public List<String> getClassificacaoGeral(String criterioDesempate){
        List<String> tabelaClassificados = new ArrayList<>();
        
        if(criterioDesempate.equals("Favoritismo")){
            this.sortAnimaisByFavoritismo();
        }else if(criterioDesempate.equals("EnergiaInicial")){            
            this.sortAnimaisByEnergiaInicial();
        }
        List<Equipa> equipas = preencherEquipas();//aproveitar o sort pela ordem de chegada
        classificarEquipas();
        
        tabelaClassificados.add("CLASSIFICACAO FINAL DA PROVA                     ");//espacos para expandir a janela do output
        
        for(Animal a:animais){
            tabelaClassificados.add("  #" + getClassificacaoDoAnimal(a) + " " +a.toString());
        }
        tabelaClassificados.add("Desempates: " + criterioDesempate);
        tabelaClassificados.add("\n");
        outputStringList.addAll(tabelaClassificados);
        outputStringList.add(addLine());
        return tabelaClassificados;
    }
    private void sortAnimaisByFavoritismo(){
        animais = animais.stream() // ordenar pela ordem contraria de prioridades da ordenacao - just because
                .sorted((a1, a2) -> a1.getNome().compareTo(a2.getNome()))
                .sorted((a1, a2) -> a1.getFavoritismo()-a2.getFavoritismo())
                .sorted((a1, a2) -> a1.getTempoChegada()-a2.getTempoChegada())
                .collect(Collectors.toList());
    }
    private void sortAnimaisByEnergiaInicial(){
        animais = animais.stream()
                .sorted((a1, a2) -> a1.getNome().compareTo(a2.getNome()))
                .sorted((a1, a2) -> a1.getEnergiaInicial()-a2.getEnergiaInicial())
                .sorted((a1, a2) -> a1.getTempoChegada()-a2.getTempoChegada())
                .collect(Collectors.toList());
    }
    private void sortAnimaisByPremios(){
        animais = animais.stream()
                .sorted((a1, a2) -> a1.getNome().compareTo(a2.getNome()))
                .sorted((a1, a2) -> a1.getFavoritismo()-a2.getFavoritismo())
                .sorted((a1, a2) -> a2.getPremiosTotalValue()-a1.getPremiosTotalValue())
                .collect(Collectors.toList());
    }
    
    public List<String> getTabelaPremios(){
        List<String> tabelaPremios = new ArrayList<>();
        
        sortAnimaisByPremios();
        
        tabelaPremios.add("TABELA DE PRÉMIOS");
        
         for(Animal a:animais)
        {
            if(a.getPremiosTotalValue()==0){
                continue;
            }
            tabelaPremios.add("\n"+a.getNome() + " " + a.getPremiosTotalValue());
            
            for(Premio p:a.getPremios()){
                tabelaPremios.add("    " + p.toString());
            }
        }
        outputStringList.addAll(tabelaPremios);
        outputStringList.add(addLine());
        return tabelaPremios;
    }
    
    public String getBackgroundImagePNG(int idPosicao){
        if(animais.isEmpty()){
            return Terreno.getTerreno().getIcon();}
        else{
            return Terreno.getTerreno().getIcon(idPosicao);}
    }
    public int getClassificacaoDoAnimal(Animal animal){
        return animais.indexOf(animal)+1;
    }
    
    public List<String>getClassificacaoEquipas(){
        List<String> out = new ArrayList<>();

        out.add("CLASSIFICACAO POR EQUIPAS");
        
        Collections.sort(equipas);
        
        for(Equipa e:equipas)
        {
            out.add( "#"+ (equipas.indexOf(e)+1) + " " + e.toString());
        }
        outputStringList.addAll(out);
        outputStringList.add(addLine());
        escreverResultados("Out.txt");
        return out;
    }
    private List<Equipa> preencherEquipas(){
        equipas = new ArrayList<>();
        animais.stream().forEach((a) -> equipas.add(new Equipa(a.getID(),a.getEspecie())));
        return equipas;
    }
    private void classificarEquipas()
    {
        this.sortAnimaisByFavoritismo();
        for(Animal a:animais){
            for(Equipa e:equipas)
            {
                if(a.getID() == e.getID()){
                    e.addPoints(animais.size()-getClassificacaoDoAnimal(a)+1);
                    break;
                }
            }
        }
    }
        
    public void escreverResultados(String filename){
        try{
            File outputFile = new File(filename);
            FileWriter escritor = new FileWriter(outputFile);
            for(String s:outputStringList)
            {
                escritor.write(s + "\n");
            }
            escritor.close();
        }catch(Exception e){
            System.out.println(e);
        }
    }

    private void lerFicheiro(File ficheiroInicial) throws IOException{ // adicionar throws "MinhaException"  //  if() throw new MinhaException(nr linha...)
        
        ValidacaoDoInput.getValidacao().verificaFicheiro(ficheiroInicial);
        
        Integer numeroAnimais;
        Integer numeroPremios;
        Integer numeroFenomenos;
        
        Scanner leitorFicheiro = new Scanner(ficheiroInicial);
        
        String cabecalho[] = leitorFicheiro.nextLine().split(":");
        numeroAnimais=Integer.parseInt(cabecalho[0]);
        numeroPremios=Integer.parseInt(cabecalho[1]);
        numeroFenomenos=Integer.parseInt(cabecalho[2]);

        String linha;
        String[] dados;

        while(leitorFicheiro.hasNextLine() && numeroAnimais>0) {//Leitura dos animais

            linha = leitorFicheiro.nextLine();
            dados = linha.split(":");

            String nome = dados[0];
            int idEspecie = Integer.parseInt(dados[2]);
            String especie = especies.getEspecieByID(Integer.parseInt(dados[2]));
            int velocidadeBase = Integer.parseInt(dados[3]);
            int energiaInicial = Integer.parseInt(dados[4]);
            int inteligencia = Integer.parseInt(dados[5]);

            animais.add(AnimalFactory.criaAnimal(nome, idEspecie, especie, velocidadeBase, energiaInicial, inteligencia));
            numeroAnimais--;
        }
        while(leitorFicheiro.hasNextLine() && numeroPremios>0) {//Leitura dos premios
            linha = leitorFicheiro.nextLine();
            dados = linha.split(":");

            String nome = dados[0];
            int valor = Integer.parseInt(dados[1]);
            int posicao = Integer.parseInt(dados[2]);
            boolean acumulavel = dados[3].toCharArray()[0]== 'S' ? true : false;

            premios.add(new Premio(nome, valor, posicao, acumulavel));
            numeroPremios--;
        }
        while(leitorFicheiro.hasNextLine() && numeroFenomenos>0) {//Leitura dos animais
            linha = leitorFicheiro.nextLine();
            dados = linha.split(":");
            int idFenomeno = Integer.parseInt(dados[0]);
            int tempo = Integer.parseInt(dados[1]);
            
            Fenomeno.getInstance().addFenomenos(tempo, idFenomeno);
            
            numeroFenomenos--;
        }
        linha = leitorFicheiro.nextLine();
        Terreno.getTerreno().setTerreno(linha.toCharArray());

        leitorFicheiro.close();
    }
        
    public void iniciaJogo(File ficheiroInicial,int comprimento) throws IOException{
        comprimentoPista=comprimento;
        tempCount=0;
        equipas = new ArrayList<>();
        animais = new ArrayList<>();
        premios = new ArrayList<>();
        especies = new Especies();
        
        outputStringList = new ArrayList<>();
        
        lerFicheiro(ficheiroInicial);
        
        participantsCount = animais.size();
    }
    
    public boolean processaTurno(){
        
        for(Animal animal:animais){
            //animal.debug();
            Fenomeno.getInstance().aplicaFenomeno(animal,tempCount);
            //animal.debug();
            if(animal.move(comprimentoPista-1,Terreno.getTerreno().getTerrenoByPosition(animal.getPosicao()))){
                participantsCount--;
                animal.setTempoChegada(tempCount);
            }
            //animal.debug();
            Fenomeno.getInstance().reverterAnimal(animal,tempCount);
            //animal.debug();
        }
        if(participantsCount>0)
        {
            tempCount++;
            return true;
        }else //Last processaTurno call
        {
            processaPremios();
            return false;
        }
    }
    private void processaPremios(){
        sortAnimaisByFavoritismo();//assumindo que este e o criterio de desempate para a classificacao
        for(Premio p:premios)//Parte 1 // atribui todos os prémios regardless se são acumulaveis ou não
        {
            for(Animal a:animais){
                if(p.getposicao() == getClassificacaoDoAnimal(a)){
                    a.addPremio(p);
                    break;
                }
            }
        }
        for(Animal a : animais){// Parte 2 // Eliminação dos prémios não acumuláveis
            if(a.checkPremiosAcumulaveis()){
                a.removePremiosNaoAcumulaveis();
            }else{
               a.reterOMaiorPremio();
            }
        }
    }
    
    private String addLine(){
        return "-----\n";
    }
    public Simulador(){
        animais = new ArrayList<>();// sera chamado em getBackgroundImagePNG. Caso nao esteja inicializada -> NullPointerException
    }
}