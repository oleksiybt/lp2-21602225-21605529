package pt.ulusofona.lp2.jungleGame.Animais;

import pt.ulusofona.lp2.jungleGame.Animal;
import pt.ulusofona.lp2.jungleGame.Terreno;


public class Leao  extends Animal{
    
    private boolean passouPrimeiroTempo;
    
    
    @Override
    public boolean move(int maxPosicao, char terreno){
        if(passouPrimeiroTempo)
            passouPrimeiroTempo=!passouPrimeiroTempo;
        else
        if(posicao!=maxPosicao)
        {
            int distancia = (int)(velocidadeBase * energiaActual * 0.5 + 1 - Terreno.getTerreno().getAtrito(terreno));
            if(ProcessaEnergia(distancia))
            {
                if(posicao+distancia<maxPosicao)//11 max & 12 finish line // looks like finish is on 11 page 19/27
                {
                    posicao += distancia;
                    //debug(distancia);
                    ConsomeEnergia(distancia);
                }
                else
                {
                    posicao = maxPosicao;
                    return true;
                }
                return false;
            }
        }
        return false;
    }
    
    
    
    
    public Leao(String nome, int id, String especie ,int velocidadeBase ,int energia, int inteligencia){
        super(nome, id, especie ,velocidadeBase ,energia, inteligencia);
        passouPrimeiroTempo=false;
    }
}
