package pt.ulusofona.lp2.jungleGame.Animais;

import pt.ulusofona.lp2.jungleGame.Animal;
import pt.ulusofona.lp2.jungleGame.Terreno;


public class Tartaruga  extends Animal{
    
    private boolean hasMoved;
    
    @Override
    public boolean move(int maxPosicao, char terreno){
        if(posicao!=maxPosicao)
        {
            int distancia = (int)(velocidadeBase * energiaActual * 0.5 + 1);
            distancia += Terreno.getTerreno().getTerrenoByPosition(posicao)=='A'? modificadorMovimento(distancia) : Terreno.getTerreno().getAtrito(terreno);
            if(ProcessaEnergia(distancia))
            {
                if(posicao+distancia<maxPosicao)
                {
                    posicao += distancia;
                    ConsomeEnergia(distancia);
                }
                else
                {
                    posicao = maxPosicao;
                    return true;
                }
                return false;
            }
        }
        return false;
    }
    
    
    
    private int modificadorMovimento(int distanciaSalto){
        int saltoBonus=0;
        char[] terreno = Terreno.getTerreno().getTodoTerreno();
        if(distanciaSalto+ posicao<terreno.length){
            for(; distanciaSalto >= 0 ; distanciaSalto--){
                if(terreno[posicao+distanciaSalto]=='A'){
                    saltoBonus++;
                }
            }
        }
        return saltoBonus;
    }
    
    
    
    public Tartaruga(String nome, int id, String especie ,int velocidadeBase ,int energia, int inteligencia){
        super(nome, id, especie ,velocidadeBase ,energia, inteligencia);
        hasMoved=false;
    }
}
