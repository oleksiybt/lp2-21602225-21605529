package pt.ulusofona.lp2.jungleGame.Animais;

import pt.ulusofona.lp2.jungleGame.Animal;


public class Voador extends Animal{
    
    
    @Override
    public boolean move(int maxPosicao, char terreno){
        if(posicao!=maxPosicao)
        {
            int distancia = (int)(velocidadeBase * energiaActual * 0.5 + 1);
            
            if(ProcessaEnergia(distancia))
            {
                if(posicao+distancia<maxPosicao)//11 max & 12 finish line
                {
                    posicao += distancia;
                    //debug(distancia);
                    ConsomeEnergia(distancia);
                }
                else
                {
                    posicao = maxPosicao;
                    return true;
                }
                return false;
            }
        }
        return false;
    }
    
    
    
    
    
    
    
    
    
     public Voador(String nome, int id, String especie ,int velocidadeBase ,int energia, int inteligencia){
        super(nome, id, especie, velocidadeBase, energia, inteligencia);
    }
    
}
