package pt.ulusofona.lp2.jungleGame.Animais;

import java.util.function.Function;
import pt.ulusofona.lp2.jungleGame.Animal;
import pt.ulusofona.lp2.jungleGame.Terreno;



public class Girafa extends Animal{
    
    
    @Override
    public boolean move(int maxPosicao, char terreno){
        if(posicao!=maxPosicao)
        {
            int distancia = (int)(velocidadeBase * energiaActual * 0.5 + 1 - Terreno.getTerreno().getAtrito(terreno)*modificadorMovimento(terreno));
            if(ProcessaEnergia(distancia))
            {
                if(posicao+distancia<maxPosicao)//11 max & 12 finish line
                {
                    posicao += distancia;
                    //debug(distancia);
                    ConsomeEnergia(distancia);
                }
                else
                {
                    posicao = maxPosicao;
                    return true;
                }
                return false;
            }
        }
        return false;
    }
    
    
    
    private int modificadorMovimento(char terreno){
        if(terreno=='R')
            return 2;
        else return 1;
    }
    
    
    public Girafa(String nome, int id, String especie ,int velocidadeBase ,int energia, int inteligencia){
        super(nome, id, especie ,velocidadeBase ,energia,inteligencia);
    }
    
}
