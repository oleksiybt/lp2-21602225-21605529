
package pt.ulusofona.lp2.jungleGame;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class ValidacaoDoInput{
    
    private static ValidacaoDoInput instance = new ValidacaoDoInput();
    
   
    
    public void verificaFicheiro(File ficheiroInicial) throws IOException{
        int headerLength=3;
        int animaisLength=6;
        int premiosLength=4;
        int fenomenosLenght=2;
        Integer[] linhasDeInput = new Integer[3];
        Scanner leitorFicheiro = new Scanner(ficheiroInicial);
        
        headerCheck(leitorFicheiro, headerLength, linhasDeInput);
        animaisCheck(leitorFicheiro, animaisLength, linhasDeInput);
        premiosCheck(leitorFicheiro, premiosLength, linhasDeInput);
        fenomenosCheck(leitorFicheiro, fenomenosLenght, linhasDeInput);
        terrenoCheck(leitorFicheiro, linhasDeInput);
    }
        
    
    public void headerCheck(Scanner leitorFicheiro, int lenght, Integer[] numeroLinhas) throws InvalidSimulatorInputException{
        String line = leitorFicheiro.nextLine();
        String header[] = line.split(":");
        if(header.length>lenght||header.length<lenght || line.contains(".;,")){
            throw new InvalidSimulatorInputException(1);
        }
        numeroLinhas[0]=Integer.parseInt(header[0]);
        numeroLinhas[1]=Integer.parseInt(header[1]);
        numeroLinhas[2]=Integer.parseInt(header[2]);
        
    }
    private void animaisCheck(Scanner leitorFicheiro, int lenght, Integer[] numeroLinhas) throws InvalidSimulatorInputException{
        int linhaAtual=2;
        int tempNumeroLinhas = numeroLinhas[0];
        for(;tempNumeroLinhas != 0;tempNumeroLinhas--,linhaAtual++){
            String line = leitorFicheiro.nextLine();
            String corpo[] = line.split(":");
            validacaoBasica(line, corpo, lenght, linhaAtual);
            if(!contemSoLetras(corpo[0]) || !contemSoNumeros(corpo[1]) || !contemSoNumeros(corpo[2]) || !contemSoNumeros(corpo[3]) || !contemSoNumeros(corpo[4]) || !contemSoNumeros(corpo[5])){
                throw new InvalidSimulatorInputException(linhaAtual);
            }
        }
        
    }
    private void premiosCheck(Scanner leitorFicheiro, int lenght, Integer[] numeroLinhas) throws InvalidSimulatorInputException{

        int linhaAtual=2 + numeroLinhas[0];
        int tempNumeroLinhas = numeroLinhas[1];
        
        for(; tempNumeroLinhas!=0 ; tempNumeroLinhas--, linhaAtual++){
            String line = leitorFicheiro.nextLine();
            String corpo[] = line.split(":");
            validacaoBasica(line, corpo, lenght, linhaAtual);
            if(!contemSoLetras(corpo[0]) || !contemSoNumeros(corpo[1]) || !contemSoNumeros(corpo[2]) || !(corpo[3].equals("S") || corpo[3].equals("N"))){
                throw new InvalidSimulatorInputException(linhaAtual);
            }
        }
        
    }
    private void fenomenosCheck(Scanner leitorFicheiro, int lenght, Integer[] numeroLinhas) throws InvalidSimulatorInputException{
       
        int linhaAtual=2 + numeroLinhas[0] + numeroLinhas[1];
        int tempNumeroLinhas = numeroLinhas[2];
        
        for(; tempNumeroLinhas!=0 ; tempNumeroLinhas--, linhaAtual++){
            String line = leitorFicheiro.nextLine();
            String corpo[] = line.split(":");
            validacaoBasica(line, corpo, lenght, linhaAtual);
            if(!contemSoNumeros(corpo[0]) || !contemSoNumeros(corpo[1])){
                throw new InvalidSimulatorInputException(linhaAtual);
            }
        }
    }
    private void terrenoCheck(Scanner leitorFicheiro, Integer[] numeroLinhas) throws InvalidSimulatorInputException{
        int linhaAtual = 2 + numeroLinhas[0] + numeroLinhas[1] + numeroLinhas[2];
        String line = leitorFicheiro.nextLine();
        String corpo[] = line.split(":");
        if(!line.matches("[TALR]+")){
            throw new InvalidSimulatorInputException(linhaAtual);
        }
    }
    
    
    
    private void validacaoBasica(String line,String[] corpo, int lenght, int numeroLinha) throws InvalidSimulatorInputException{
        if(corpo.length>lenght||corpo.length<lenght || line.matches("[.;,]")){
            throw new InvalidSimulatorInputException(numeroLinha);
        }
    }
    private boolean validarNumeroOcorrencias(String line, int numOcorrencias){
        return line.split(":").length == numOcorrencias;
    }
    private boolean contemSoLetras(String string){
        //return string.contains("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
        return string.matches("[a-z A-Z]+");
    }
    private boolean contemSoNumeros(String string){
        return string.matches("[\\d]+");
    }
    
    public static ValidacaoDoInput getValidacao(){
        if(instance==null){
            instance = new ValidacaoDoInput();
        }
        return instance;
    }
    private ValidacaoDoInput(){
        
    }
    
}
