package pt.ulusofona.lp2.jungleGame;

import pt.ulusofona.lp2.jungleGame.Animais.*;

public class AnimalFactory {
    
    public static Animal criaAnimal(String nome, int id, String especie ,int velocidadeBase ,int energia, int inteligencia){

        switch (id){
            case 0:
            case 6:
                return new Voador(nome, id, especie, velocidadeBase, energia, inteligencia);
            case 1:
                return new Girafa(nome, id, especie, velocidadeBase, energia, inteligencia);
            case 2:
                return new Gorila(nome, id, especie, velocidadeBase, energia, inteligencia);
            case 4:
                return new Leao(nome, id, especie, velocidadeBase, energia, inteligencia);
            case 7:
                return new Tartaruga(nome, id, especie, velocidadeBase, energia, inteligencia);
            case 3:
            case 5:
            case 8:
            case 9:
                return new Outros(nome, id, especie, velocidadeBase, energia, inteligencia);
            default:
                throw new IllegalArgumentException("Terreno inválido ("+id+")");
        }
        
    }
    
}
